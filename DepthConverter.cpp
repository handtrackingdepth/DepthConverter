#include <numeric>
#include <opencv2/opencv.hpp>
#include "imageList.hpp"

int main(int argc, char **argv)
{
    ImageList lst(argv[1]);

    std::vector<int> indices(lst.size());
    std::iota(indices.begin(), indices.end(),0);

    std::vector<cv::Mat> converted(lst.size());
    std::transform(indices.begin(), indices.end(), converted.begin(), [&lst](int i)
    {
        cv::Mat current = lst[i];

        cv::Mat converted;
        if(current.depth() == CV_8U)
        {
            current.convertTo(converted, CV_16U, 2048.0/255.0);
        }
        else if(current.depth() == CV_16U)
        {
            current.convertTo(converted, CV_8U, 255.0/2048.0);
        }
        else
        {
            throw std::runtime_error("Only 16U or 8U images are supported");
        }

        return converted;
    });

    std::string postfix = argv[2];

    for(int i : indices)
    {
        cv::Mat c = converted[i];
        std::string name = lst.name(i);

        auto extensionLoc = name.find_last_of('.');
        auto fileNameNoExt = name.substr(0, extensionLoc);

        std::ostringstream formatter;
        formatter << fileNameNoExt << postfix << ".png";

        cv::imwrite(formatter.str(), c);
    }

    return 0;
}
