#include <fstream>
#include <iterator>
#include "imageList.hpp"

ImageList::ImageList(std::string fileName)
{
    std::ifstream reader(fileName);

    std::copy(std::istream_iterator<std::string>(reader), std::istream_iterator<std::string>(), std::back_inserter(imageFileNames));
}

cv::Mat ImageList::operator[](unsigned int index)
{
    std::string fileName = imageFileNames[index];

    cv::Mat img = cv::imread(fileName, CV_LOAD_IMAGE_ANYDEPTH);

    return img;
}

std::string ImageList::name(unsigned int index)
{
    return imageFileNames[index];
}

unsigned long ImageList::size(void)
{
    return imageFileNames.size();
}